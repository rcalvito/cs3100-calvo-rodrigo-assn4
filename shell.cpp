#include "shell.hpp"
#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <chrono>
#include <sys/wait.h>
#include <unistd.h>
#include <errno.h>
#include <iomanip>
#include <cstring>

void getWords(std::string sentence, std::vector<std::string>& vector){
	std::istringstream input(sentence);
	while(input){
	
		std::string word;
		input >> word;
		vector.push_back(word);
	}
	vector.pop_back();
	
	
}

std::vector<std::vector<std::string>> parseCommands(std::vector<std::string> command){
	int count=0;
	std::vector<std::vector<std::string>> commands;
	for(int i=0;i<(int)command.size();i++){
		if(command[i]=="|"){
			count++;
		}
	}
	commands.resize(count+1);
	int j=0;
	for(int i=0;i<=count;i++){
		for(;j<(int)command.size();j++){
			if(command[j]=="|"){
				j++;
				break;
			}
			else{
				commands[i].push_back(command[j]);

			}
		}
	}
	
	return commands;
			
	
	
}

void mulPipes(std::vector<std::vector<std::string>> commands){
	
	int p[commands.size()-1][2];
	int count=0;
	for(int i=0;i<(int)commands.size()-1;i++){
		pipe(p[i]);
	}
	while(count<(int)commands.size()){
		if(count==0){
			pid_t first = fork();
			if(first==0){
				char **args = new char*[commands[0].size()+1];
				for(int i=0;i<(int)commands[0].size();i++){
					args[i]=(char*)commands[0][i].c_str();
				}
				args[commands[0].size()] = (char*)NULL;
				close(p[0][0]);
				dup2(p[0][1],STDOUT_FILENO);
				execvp(args[0],args);
				std::cerr << args[0] << " did something wrong\n"<<strerror(errno)<<std::endl;
			}
			else{
				wait(NULL);
				close(p[0][1]);
				count++;
			}
		}
		else if(count==(int)commands.size()){
			pid_t last = fork();
			if(last==0){
				char **args = new char*[commands[count].size()+1];
				for(int i=0;i<(int)commands[count-1].size();i++){
					args[i]=(char*)commands[count-1][i].c_str();
				}
				args[commands[count-1].size()] = (char*)NULL;
				close(p[count-1][1]);
				dup2(p[count-1][0],STDIN_FILENO);
				execvp(args[0], args);
				std::cerr << args[0] << " did something wrong\n"<<strerror(errno)<<std::endl;
			}
			else{
				wait(NULL);
				count++;
			}
		}
		else{
			pid_t middle = fork();
			if(middle==0){
				char **args = new char*[commands[count].size()+1];
				for(int i=0;i<(int)commands[count].size();i++){
					args[i]=(char*)commands[count][i].c_str();
				}
				args[commands[count].size()]= (char*)NULL;
				close(p[count-1][1]);
				dup2(p[count-1][0],STDIN_FILENO);
				dup2(p[count][1], STDOUT_FILENO);
				execvp(args[0],args);
			}
			else{
				wait(NULL);
				close(p[count-1][0]);
				close(p[count][1]);
				count++;
			}
		}
	}
		
	for(int i=0;i<(int)commands.size()-1;i++){
		for(int j=0;j<2;j++){
			close(p[i][j]);
		}
	}
			
			
							
	
	
	
}

	


void runCmd(std::vector<std::string> command, std::vector<std::string> history,double& time){
	std::vector<int> pipes;
	
	for(int i=0; i<(int)command.size();i++){
		if(command[i]== "|"){
			pipes.push_back(i);
		}
	}
	if(command[0]=="ptime"){
		std::cout<<"Time executing child processes: "<<std::setprecision(4)<<time<<std::endl;
	}
	else if(command[0]=="history"){
		if(history.size()==0){
			std::cout<<"No history available"<<std::endl;
		}
		for(int i=0;i<(int)history.size();i++){
			std::cout<<i+1<<".- "<<history[i]<<std::endl;
		}
	}
	else if(command[0]=="cd"){
		if(command[1]==".."){
			std::string directory= get_current_dir_name();
			for(int i=(int)directory.length();i>0;i--){
				if(directory[i]=='/'){
					directory=directory.substr(0,i);
					break;
				}
			}
			chdir(directory.c_str());
			if(errno!=0){
				std::cerr << "cd did something wrong\n"<<strerror(errno)<<std::endl;
				errno=0;
			}
		}
		else{
			std::string directory=get_current_dir_name();
			directory+="/";
			for(int i=1;i<(int)command.size();i++){
				directory+=command[i];
				directory+="/";
			}
			chdir(directory.c_str());
			if(errno!=0){
				std::cerr << "cd did something wrong\n"<<strerror(errno)<<std::endl;
				errno=0;
			}
		}
	}
	else if(command[0][0]=='^'){
		std::string number= command[0].substr(1,command[0].length());
		int index= std::stoi(number);
		std::string newCmd=history[index-1];
		std::vector<std::string> vector;
		getWords(newCmd,vector);
		runCmd(vector,history,time);
			
	}
		
	else if(command[0]=="exit"){
		exit(0);
	}
	
	else if(pipes.size()>=1){
		std::vector<std::vector<std::string>> hola = parseCommands(command);
		auto before = std::chrono::high_resolution_clock::now();
		mulPipes(hola);
		auto after = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> dur= after-before;
		time+=dur.count();
	}
	
	else {
		if (fork()) {
			auto before = std::chrono::high_resolution_clock::now();
            wait(NULL);
            auto after = std::chrono::high_resolution_clock::now();
			std::chrono::duration<double> dur= after-before;
			time+=dur.count();
		}
         
        else {
			
            char **args = new char*[command.size()+1];
            for(int i=0;i<(int)command.size();i++){
				args[i]=(char*)command[i].c_str();
			}
            args[command.size()] = (char*)NULL;

            int good=execvp(args[0], args);
            std::cerr << args[0] << " did something wrong\n"<<strerror(errno)<<std::endl;
            if(good){
				exit(0);
			}

            
        }
    	
	}

}


std::string getInput(){
	std::string input;
	std::getline(std::cin, input);
	return input;
	
	
	

}


void runShell(){
	std::vector<std::string> words;
	std::vector<std::string> commands;
	double dur=0;
	while(true){
		std::cout<<"["<<get_current_dir_name()<<"]:";
		std::string input=getInput();
		commands.push_back(input);
		getWords(input,words);
		runCmd(words,commands,dur);
		words.clear();
	}
	
	
}

	
	
	
	
	
	
	

