#pragma once
#include <vector>
#include <sstream>
#include <chrono>

void runCmd(std::vector<std::string> command, std::vector<std::string> history, double &time);
void getWords(std::string sentence, std::vector<std::string>& vector);
std::string getInput();
void runShell();
void runPipe(std::vector<std::string> command, std::vector<int> pipes);
std::vector<std::vector<std::string>> parseCommands(std::vector<std::string> command); 
void mulPipes(std::vector<std::vector<std::string>> commands);

